# Hearthstone Indonesia - Recordare
A quasi-learning project that leverages react, redux, browserify, swagger, and restify to make isomorphic web app. This web app purpose is to view Hearthstone ID tournaments in a more granular pieces of information that are easier to digest.

### Roadmap

V0.1.0

- [ ] Make boilerplate front-end project
  - [x] Setup react-redux project structure
  - [ ] Setup front-end routing
  - [ ] Add basic bootstrap pages
  - [ ] Add basic materialize pages on another branch
  - [ ] Add state logging
  - [ ] Add versioning display
  - [ ] Setup front-end unit tests
- [ ] Make boilerplate back-end project
  - [ ] Setup restify-swagger project structure
  - [ ] Add basic endpoints
  - [ ] Add request logging
  - [ ] Setup back-end unit tests
- [ ] Integrate both boilerplate projects
- [ ] Finalize boilerplate project
  - [ ] Create comprehensive readme
  - [ ] Expose the integrated project into the wild

v1.0.0

- [ ] Create a worker that periodically syncs data from Challonge to local db
- [ ] Create Tournament list page
- [ ] Create Tournament detail page (without matches/participants)
- [ ] Render match tree in tournament detail page
- [ ] Add participant list in tournament detail page
- [ ] Add hidden participant list in tournament detail page sorted by their result
- [ ] Create Player list page
- [ ] Create Player detail page (without their records)
- [ ] Add tournament participation history in player detail page
- [ ] Add summarized record in player detail page
- [ ] Add bug reporting tool
