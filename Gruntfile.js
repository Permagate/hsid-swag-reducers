/**
 * Gruntfile
 */
'use strict';

module.exports = function(grunt) {
  grunt.initConfig({
    eslint: {
      app: {
        files: [{src: ['app/**/*.js', '!**/bundle.js']}],
        options: {
          envs: ["commonjs", "es6", "browser"]
        }
      }
    }
  });

  require('load-grunt-tasks')(grunt);

  grunt.registerTask('default', ['eslint:app']);
};
