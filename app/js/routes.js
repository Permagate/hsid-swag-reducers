/**
 * Route definition file.
 */
'use strict';
import * as React       from 'react';
import RootContainer    from './containers/root';
import HomeContainer    from './containers/content/home';
import AboutContainer   from './containers/content/about';

import { Router, Route } from 'react-router';

let RouterConfig = (
  <Router>
    <Route path="/" component={RootContainer}>
      <IndexRoute component={HomeContainer}/>
      <Route path="home" component={HomeContainer}/>
      <Route path="about" component={AboutContainer}/>
    </Route>
  </Router>
);

export default RouterConfig;
