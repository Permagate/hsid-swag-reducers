/**
 * The entry-point of front-end app.
 */
'use strict';
import * as React       from 'react';
import * as ReactRedux  from 'react-redux';
import Store            from './state-manager/store';
import RootContainer    from './containers/root';

React.render(
  <ReactRedux.Provider store={Store}>
    {() => <RootContainer/>}
  </ReactRedux.Provider>,
  document.getElementById('container')
);
