/**
 * Actions that are related with user domain.
 */
'use strict';
import * as ActionConstants from '../constants/actions';

/**
 * Set name.
 * @param {String} name The current user's name.
 */
export let setName = (name) => {
  return {
    type: ActionConstants.SET_NAME,
    data: { name }
  };
};

export default { setName };
