/**
 * Constants that contain list of action names that can be used under the app.
 */
'use strict';

/**
 * Miscellanous Actions.
 */
export const SET_NAME = 'SET_NAME';
