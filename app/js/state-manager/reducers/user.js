/**
 * User reducer.
 */
'use strict';
import * as ActionConstants from '../constants/actions';

let _setName = (state, action) => {
  return {
    ...state,
    name: action.data.name,
    ui: {
      isNamePristine: false
    }
  };
};

let defaultState = {
  name: 'Anonymous',
  ui: {
    isNamePristine: true
  }
};

let userReducer = (state = defaultState, action) => {
  switch (action.type) {
    case ActionConstants.SET_NAME:
      return _setName(state, action);
    default:
      return state;
  }
};

export default userReducer;
