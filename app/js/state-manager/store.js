/**
 * The single store that contains Application State Tree (or collection of reducers if you prefer that term).
 */
'use strict';
import * as Redux from 'redux';
import user       from './reducers/user';

let rootReducer = Redux.combineReducers({
  user
});
let Store = Redux.createStore(rootReducer);

export default Store;
