/**
 * The single store that contains Application State Tree (or collection of reducers if you prefer that term).
 */
'use strict';
import User from './actions/user';

let Actions = {
  User
};

export default Actions;
