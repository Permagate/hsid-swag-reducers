/**
 * The container that renders homepage content.
 */
'use strict';
import * as React      from 'react';
import * as ReactRedux from 'react-redux';
import Actions         from '../../../state-manager/actions';

let mapStateToProps = (state) => {
  return {
    user: state.user
  };
};

let mapDispatcherToProps = (dispatch) => {
  return {
    onChangeName(event) {
      dispatch(Actions.User.setName(event.target.value));
    }
  };
};

let HomeContainer = React.createClass({
  deduceName() {
    if(this.props.user.ui.isNamePristine === true) {
      return '';
    }
    return this.props.user.name;
  },
  render() {
    return (
      <div id="content-home">
        <input onChange={this.props.onChangeName}
               placeholder="Type here to change your name!"
               type="text"
               value={this.deduceName()}
        />
      </div>
    );
  }
});

export default ReactRedux.connect(mapStateToProps, mapDispatcherToProps)(HomeContainer);
