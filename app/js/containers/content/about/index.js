/**
 * The container that renders homepage content.
 */
'use strict';
import * as React from 'react';

let AboutContainer = React.createClass({
  render() {
    return (
      <div id="content-about">
        <p>{`Made in Indonesia by Permagate`}</p>
      </div>
    );
  }
});

export default AboutContainer;
