/**
 * The entry-point of front-end app.
 */
'use strict';
import * as React        from 'react';
import HeaderContainer   from './header';
import NotFoundContainer from './_meta/404';

let MainContainer = React.createClass({
  render() {
    return (
      <div id="main-container">
        <HeaderContainer/>
        <div id="content-container">
          {this.props.children || <NotFoundContainer/>}
        </div>
      </div>
    );
  }
});

export default MainContainer;
