/**
 * The container that renders homepage content.
 */
'use strict';
import * as React from 'react';

let NotFoundContainer = React.createClass({
  render() {
    return (
      <div id="content-not-found">
        <p>{`You found missingno! Go back to where you come from!`}</p>
      </div>
    );
  }
});

export default NotFoundContainer;
