/**
 * The component that renders the page header.
 */
'use strict';
import * as React      from 'react';
import * as ReactRedux from 'react-redux';

let mapStateToProps = (state) => {
  return {
    user: state.user
  };
};

let mapDispatcherToProps = () => {
  return {};
};

let HeaderContainer = React.createClass({
  render() {
    return (
      <header id="header-container">
        <h1>{`Hello ${this.props.user.name}!`}</h1>
      </header>
    );
  }
});

export default ReactRedux.connect(mapStateToProps, mapDispatcherToProps)(HeaderContainer);
